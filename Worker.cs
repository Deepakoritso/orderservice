using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Data;
using Aspose.Cells.Utility;
using Aspose.Cells;

namespace OrderService
{
    #region Orders-Report

    #region orrders
    public class orrders
    {


        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]

        public string _id { get; set; }

        public string date { get; set; }

        public DateTime createdAt { get; set; }

        public DateTime updatedAt { get; set; }
        public List<Orders> orders { get; set; }


    }
    #endregion

    #region orders
    public class Orders
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public List<ShopTypeCategories> shopTypeCategories { get; set; }
    }
    #endregion

    #region Shop-Type-category
    public class ShopTypeCategories
    {
        public int shopTypeId { get; set; }
        public string shopTypeName { get; set; }
        public List<StoreOrders> storeOrders { get; set; }
    }
    #endregion

    #region Store-Orders
    public class StoreOrders
    {
        public int storeId { get; set; }
        public string storeName { get; set; }
        public List<CustomerOrders> customerOrders { get; set; }
    }
    #endregion

    #region Customer-Orders
    public class CustomerOrders
    {
        public DateTime orderTimeStamp { get; set; }
        public int customerId { get; set; }
        public int personId { get; set; }
        public string orderId { get; set; }
        public int orderAmount { get; set; }
        public string orderStatus { get; set; }
    }
    #endregion

    #endregion


    public class Values
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string date { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public int shopTypeId { get; set; }
        public string shopTypeName { get; set; }
        public int storeId { get; set; }
        public string storeName { get; set; }
        public DateTime orderTimeStamp { get; set; }
        public int customerId { get; set; }
        public int personId { get; set; }
        public string orderId { get; set; }
        public int orderAmount { get; set; }
        public string orderStatus { get; set; }
        public string orderType { get; set; }
        public string orderStatusDes { get; set; }
        public string orderNumber { get; set; }
        public string uri { get; set; }
        public string path { get; set; }
    }

    public class ValuesList
    {
        public List<Values> ValueData { get; set; }
    }


    public class OrderResponse
    {
        public string status { get; set; }
        public object data { get; set; }
        public string message { get; set; }

    }




    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly orrders _content;
        public readonly Values _values;
        //private StringValue cellReference;
        ArrayList nlist = new ArrayList();

       

        public Worker(ILogger<Worker> logger, IOptions<orrders> content, IOptions<Values> values)
        {
            _logger = logger;
            _content = content.Value;
            _values = values.Value;
        }


        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service Started: {time}", DateTimeOffset.Now);
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service Stopped: {time}", DateTimeOffset.Now);
            return base.StopAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                call();
                await Task.Delay(1000 * 50, stoppingToken);
            }
        }

        private async void call()
        {
            await Index();
        }

        public async Task<List<orrders>> Index()
        {
            List<orrders> reservationList = new List<orrders>();
            using (var httpClient = new HttpClient())
            {
                #region Prepare Http payload for OrderReport Request
                var uri = _values.uri;
                Uri orderReportRqUri = new Uri(uri);
                var payload = "{\"date\": \"" + _content.date.ToString().Trim() + "\"}";
                //Log.Information($"Payload: {payload}");
                HttpContent orderReportRq = new StringContent(payload, Encoding.UTF8, "application/json");
                httpClient.DefaultRequestHeaders.Add("bucketToken", "22E97B9F-14D7-4637-91A9-4ED5D6E4A0FA");

                #endregion
                //Console.WriteLine();
                using (var response = await httpClient.PostAsync(orderReportRqUri, orderReportRq))
                {
                    var apiResponse = await response.Content.ReadAsStringAsync();
                    var result = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderResponse>(apiResponse);

                    string str = apiResponse;


                    //string str = "{\"orders\":{\"shopTypeCategories\":[{\"shopTypeId\":34,\"shopTypeName\":\"Musical Instruments & Accessories\",\"storeOrders\":[{\"storeId\":3,\"storeName\":\"Vicky Store\",\"customerOrders\":[{\"orderTimeStamp\":\"2022-03-01T11:52:01.379Z\",\"customerId\":61,\"orderId\":1619,\"orderAmount\":200,\"orderStatus\":\"Pending\",\"orderType\":\"COD\",\"orderStatusDes\":\"Pending\",\"orderNumber\":\"OD00001929\"}]}]}]}}";
                    string path = _values.path;

                    JObject json = JObject.Parse(str);

                    ValuesList tmList = new ValuesList();
                    tmList.ValueData = new List<Values>();

                    Values tm = new Values();


                    foreach (var a in json["data"])
                    {

                        foreach (var b in a["orders"]["shopTypeCategories"])
                        {
                            _values.shopTypeId = (int)b["shopTypeId"];
                            _values.shopTypeName = (string)b["shopTypeName"];
                            //Console.WriteLine("shopTypeId" + "  :  " + _values.shopTypeId);
                            //Console.WriteLine("shopTypeName" + "  :  " + _values.shopTypeName);
                            tm.shopTypeName = _values.shopTypeName;

                            foreach (var c in b["storeOrders"])
                            {
                                _values.storeId = (int)c["storeId"];
                                _values.storeName = (string)c["storeName"];
                                tm.storeId = _values.storeId;
                                tm.storeName = _values.storeName;

                                foreach (var d in c["customerOrders"])
                                {
                                    _values.orderTimeStamp = (DateTime)d["orderTimeStamp"];
                                    _values.customerId = (int)d["customerId"];
                                    _values.orderId = (string)d["orderId"];
                                    _values.orderAmount = (int)d["orderAmount"];
                                    _values.orderStatus = (string)d["orderStatus"];
                                    _values.orderType = (string)d["orderType"];
                                    _values.orderStatusDes = (string)d["orderStatusDes"];
                                    _values.orderNumber = (string)d["orderNumber"];

                                    tm.orderTimeStamp = _values.orderTimeStamp;
                                    tm.customerId = _values.customerId;
                                    tm.orderId = _values.orderId;
                                    tm.orderAmount = _values.orderAmount;
                                    tm.orderStatus = _values.orderStatus;
                                    tm.orderType = _values.orderType;
                                    tm.orderStatusDes = _values.orderStatusDes;
                                    tm.orderNumber = _values.orderNumber;

                                    nlist.Add(tm.shopTypeId);
                                    nlist.Add(tm.shopTypeName);
                                    nlist.Add(tm.storeId);
                                    nlist.Add(tm.storeName);
                                    nlist.Add(tm.orderTimeStamp);
                                    nlist.Add(tm.customerId);
                                    nlist.Add(tm.orderId);
                                    nlist.Add(tm.orderAmount);
                                    nlist.Add(tm.orderStatus);
                                    nlist.Add(tm.orderType);
                                    nlist.Add(tm.orderStatusDes);
                                    nlist.Add(tm.orderNumber);



                                }
                            }
                        }
                    }
                    var myData = result.data;
                    using (StreamWriter writer = new StreamWriter(@"D:\Workspace\orderservice\O.csv"))
                    {
                        writer.WriteLine(myData.ToString());
                    }

                }
            }
                return reservationList;
                
            
        }
    }

}







































































//                                        //Console.WriteLine(json["data"]);
//                                        Sheet sheet = new Sheet()
//                                        {
//                                            //Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
//                                            Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
//                                            SheetId = 1,
//                                            Name = tm.shopTypeName,
//                                        };
//                                        sheets.Append(sheet);

//                                        // Get the sheetData cell table.
//                                        SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

//                                        // Add a row to the cell table.
//                                        Row row;
//                                        row = new Row() { RowIndex = 1 };
//                                        sheetData.Append(row);
                                        

//                                        // In the new row, find the column location to insert a cell in A1.  
//                                        Cell refCell = null;
//                                        foreach (Cell cell in row.Elements<Cell>())
//                                        {
//                                            if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
//                                            {
//                                                refCell = cell;
//                                                break;
//                                            }
//                                        }
//                                        Cell ce = new Cell { CellReference = cellReference };
//                                        row.InsertBefore(ce, refCell);

//                                        Cell cell1 = new Cell()
//                                        {
//                                            CellReference = cellReference,
//                                            DataType = CellValues.Number,
//                                            CellValue = new CellValue("storeId")

//                                        };                                       
//                                       row.InsertBefore(cell1, refCell);

//                                        Cell cell2 = new Cell()
//                                        {
//                                            CellReference = cellReference,
//                                            DataType = CellValues.String,
//                                            CellValue = new CellValue("storeName")
//                                        };
//                                        row.InsertBefore(cell2, refCell);
                                       

//                                       Cell cell3 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderId")
//                                       };
//                                       row.InsertBefore(cell3, refCell);

//                                       Cell cell4 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderAmount")
//                                       };
//                                       row.InsertBefore(cell4, refCell);


//                                       Cell cell5 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderType")
//                                       };
//                                       row.InsertBefore(cell5, refCell);

//                                       Cell cell6 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderStatus")
//                                       };
//                                       row.InsertBefore(cell6, refCell);

//                                       Cell cell7 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderStatusDes")
//                                       };
//                                       row.InsertBefore(cell7, refCell);

//                                       Cell cell8 = new Cell()
//                                       {
//                                           CellReference = cellReference,
//                                           DataType = CellValues.String,
//                                           CellValue = new CellValue("orderNumber")
//                                       };
//                                       row.InsertBefore(cell8, refCell);

                                       
//                                        worksheet.Save();
//                                        nlist.Add(tm.shopTypeId);
//                                        nlist.Add(tm.shopTypeName);
//                                        nlist.Add(tm.storeId);
//                                        nlist.Add(tm.storeName);
//                                        nlist.Add(tm.orderTimeStamp);
//                                        nlist.Add(tm.customerId);
//                                        nlist.Add(tm.orderId);
//                                        nlist.Add(tm.orderAmount);
//                                        nlist.Add(tm.orderStatus);
//                                        nlist.Add(tm.orderType);
//                                        nlist.Add(tm.orderStatusDes);
//                                        nlist.Add(tm.orderNumber);
                                       
                                      
//                                    }
//                                }

//                            }
//                        }

//                        int stride = nlist.Count/12;
//                        int i = 0;
//                        for (int row = 0; row < stride; row++)
//                        {
//                            for(int col = 0; col < 12; col++)
//                            {
//                                Console.Write($"{nlist[i]}" + "\t");
//                                i++;
//                            }
//                            Console.WriteLine("\n");
//                        }
                        
//                        workbookpart.Workbook.Save();
//                        spreadsheetDocument.Close();
//                    }
//                        return reservationList;
//                }
//            }
//        }
//    }
//}
































































// Add the cell to the cell table at A1.
//    Cell newCell = new Cell()

//    { CellReference = "A1"


//    newCell.CellValue = new CellValue(tm.orderType);

//    newCell.DataType = new EnumValue<CellValues>(CellValues.String)
//};

//row.InsertBefore(newCell, refCell);
//newCell.CellValue = new CellValue("100");























//        //public static void CreateExcelFile(string filepath, string sheetName)
//        //{
//        //    // Create a spreadsheet document by supplying the filepath.
//        //    SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook);
//        //    // Add a WorkbookPart to the document.
//        //    WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
//        //    workbookpart.Workbook = new Workbook();
//        //    // Add a WorksheetPart to the WorkbookPart.
//        //    WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
//        //    worksheetPart.Worksheet = new Worksheet(new SheetData());
//        //    // Add Sheets to the Workbook.
//        //    Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.
//        //    AppendChild<Sheets>(new Sheets());
//        //    // Append a new worksheet and associate it with the workbook.
//        //    Sheet sheet = new Sheet()
//        //    {
//        //        Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
//        //        SheetId = 1,
//        //        Name = sheetName
//        //    };
//        //    sheets.Append(sheet);
//        //    //Save & close
//        //    workbookpart.Workbook.Save();
//        //    spreadsheetDocument.Close();
//        //}

//    }
//}










































































































































































































//foreach (var e in json["data"])
//{
//    _values._id = (string)e["_id"];
//    _values.date = (string)e["date"];
//    _values.createdAt = (DateTime)e["createdAt"];
//    _values.updatedAt = (DateTime)e["updatedAt"];
//    writer.WriteLine("_id " +  " : "  + _values._id);
//    writer.WriteLine("date" + "  :  " + _values.date);
//    writer.WriteLine("createdAt" + "  :  " + _values.createdAt);
//    writer.WriteLine("updatedAt" + "  :  " + _values.updatedAt);
//    tm._id = _values._id;
//    tm.date = _values.date;
//    tm.createdAt = _values.createdAt;
//    tm.updatedAt = _values.updatedAt;

//    foreach (var x in e["orders"]["shopTypeCategories"])
//    {
//        _values.shopTypeId = (int)x["shopTypeId"];
//        _values.shopTypeName = (string)x["shopTypeName"];
//        writer.WriteLine("shopTypeId" + "  :  " + _values.shopTypeId);
//        writer.WriteLine("shopTypeName" + "  :  " + _values.shopTypeName);
//        tm.shopTypeId = _values.shopTypeId;
//        tm.shopTypeName = _values.shopTypeName;
//        ArrayList al = new ArrayList();
//        al.Add(tm.shopTypeName);



//        foreach (var a in x["storeOrders"])
//        {
//            _values.storeId = (int)a["storeId"];
//            _values.storeName = (string)a["storeName"];
//            writer.WriteLine("storeId" + "  :  " + _values.storeId);
//            writer.WriteLine("storeName" + "  :  " + _values.storeName);
//            tm.storeId = _values.storeId;
//            tm.storeName = _values.storeName;

//            foreach (var b in a["customerOrders"])
//            {
//                _values.orderTimeStamp = (DateTime)b["orderTimeStamp"];
//                _values.customerId = (int)b["customerId"];
//                _values.personId = (int)b["personId"];
//                _values.orderId = (string)b["orderId"];
//                _values.orderAmount = (int)b["orderAmount"];
//                _values.orderStatus = (string)b["orderStatus"];
//                writer.WriteLine("orderTimeStamp" + "  :  " + _values.orderTimeStamp);
//                writer.WriteLine("customerId" +"  :  " + _values.customerId);
//                writer.WriteLine("personId" +  "  :  " + _values.personId);
//                writer.WriteLine("orderId" + "  :  " + _values.orderId);
//                writer.WriteLine("orderAmount" + "  :  " + _values.orderAmount);
//                writer.WriteLine("orderStatus" +"  :  " + _values.orderStatus);
//                tm.orderTimeStamp = _values.orderTimeStamp;
//                tm.customerId = _values.customerId;
//                tm.personId = _values.personId;
//                tm.orderId = _values.orderId;
//                tm.orderAmount = _values.orderAmount;
//                tm.orderStatus = _values.orderStatus;

//                tmList.ValueData.Add(tm);
//            }
//        }
//    }

//}






//var myData = result.data;
//using (StreamWriter writer = new StreamWriter(@"D:\Workspace\orderservice\Orders-Report.csv"))
//{
//    writer.WriteLine(myData);
//}

















//foreach (var e in json["data"]["orders"])
//{
//    Console.WriteLine(e);


//}































































































































































//ExcelFile workbook = new ExcelFile();
//ExcelWorksheet worksheet = workbook.Worksheets.Add("Users");


//worksheet.Cells[0, 0].Value = "_id";
//worksheet.Cells[0, 1].Value = "date";
//worksheet.Cells[0, 2].Value = "createdAt";
//worksheet.Cells[0, 3].Value = "updatedAt";

//int row = 0;
//foreach(orrders user in users)
//{
//    worksheet.Cells[++row, 0].Value = user._id;
//    worksheet.Cells[row, 1].Value = user.date;
//    worksheet.Cells[row, 2].Value = user.createdAt;
//    worksheet.Cells[row, 3].Value = user.updatedAt;
//}
//workbook.Save(@"D:\Workspace\orderservice\Orders-Report.xlsx");

















//var rrr = json.parse(result);
// new rr = JSON.parse(result);

//Model model = new Model(result);
//Model res = model;
//string _id = res._id;
//string date = res.date;
//DateTime createdAt = res.createdAt;
//DateTime updatedAt = res.updatedAt;
//int shopTypeId = res.shopTypeId;
//string shopTypeName = res.shopTypeName;
//int storeId = res.storeId;
//string storeName = res.storeName;
//DateTime orderTimeStamp = res.orderTimeStamp;
//int customerId = res.customerId;
//int personId = res.personId;
//string orderId = res.orderId;
//int orderAmount = res.orderAmount;
//string orderStatus = res.orderStatus;


// Console.WriteLine(apiResponse);
//byte[] res = apiResponse;
//string result = System.Text.Encoding.UTF8.GetString(res);
//Console.WriteLine(result);
//var result = await response.Content.ReadAsStringAsync();

//var root = JsonSerializer.Deserialize<OrderResponse>(apiResponse);
//Console.WriteLine(result.data);

//Log.Information("below data is the response of api");
////Log.Information(root);
//Log.Information("Api Response data Ended");


























































































































//    var values = new Dictionary<string, string>{
//    { "productId", "1" },
//    { "productKey", "Abc6666" },
//    { "userName", "OPPO" },
//};
//    var json = JsonConvert.SerializeObject(values, Formatting.Indented);
//    // var content = new FormUrlEncodedContent(json);
//    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//var response = await client.PostAsync("http://172.116.12.172:8014/iadmin/validate", json);
//    var responseString = await response.Content.ReadAsStringAsync();
//return Ok(responseString);










//SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(@"D:\Workspace\orderservice\Orders-Report.xlsx", SpreadsheetDocumentType.Workbook);
//WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
//workbookPart.Workbook = new Workbook();
//workbookPart.Workbook.AppendChild(new Sheets());
//WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
//worksheetPart.Worksheet = new Worksheet(new SheetData());

//Sheet sheet = new Sheet
//{
//    Id = workbookPart.GetIdOfPart(worksheetPart),
//    SheetId = 1,
//    Name = "TestSheet"
//};
//spreadsheetDocument.WorkbookPart.Workbook.Sheets.Append(sheet);

//Row row = new Row();

//SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
//sheetData.Append(row);

//Cell cell = new Cell
//{
//    CellReference = "A1",
//    CellValue = new CellValue("Hello Excell..!!"),
//    DataType = CellValues.String
//};
//row.Append(cell);

//workbookPart.Workbook.Save();
//spreadsheetDocument.Close();
















//string filename = @"D:\Workspace\orderservice\Orders-Report.json";
//string jsonText = File.ReadAllText(filename);
//var dat = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderResponse>(jsonText);